#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
    char info;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;


Arvore*  cria_arvore_vazia (void);
Arvore*  constroi_arvore (char c, Arvore* e, Arvore* d);
int      verifica_arvore_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
int      pertence_arvore (Arvore* a, char c);
void     imprime_arvore (Arvore* a);
Arvore * cria_espelho(Arvore * arvore_a);
int eh_espelho (Arvore* a, Arvore* b);


Arvore* cria_arvore_vazia (void) {
    return NULL;
}

Arvore* constroi_arvore (char c, Arvore* e, Arvore* d) {
    Arvore* a = (Arvore*)malloc(sizeof(Arvore));
    a->info = c;
    a->esq = e;
    a->dir = d;
    return a;
}

int verifica_arvore_vazia (Arvore* a) {
    return (a == NULL);
}

Arvore* libera_arvore (Arvore* a) {
    if (!verifica_arvore_vazia(a)) {
        libera_arvore (a->esq);
        libera_arvore (a->dir);
        free(a);
    }
    return NULL;
}

int conta_arvore (Arvore* a){
	if(!a) return(0);
	return(1 + conta_arvore(a->esq) + conta_arvore(a->dir));
}

void imprime_arvore (Arvore* a){
	if(!a) return;
	imprime_arvore(a->esq);
	imprime_arvore(a->dir);
	printf("%c ", a->info);
}

Arvore * cria_espelho(Arvore * arvore_a) {
    if (arvore_a == NULL)
        return NULL;
    return constroi_arvore(arvore_a->info, cria_espelho(arvore_a->dir), cria_espelho(arvore_a->esq));
}

int eh_espelho (Arvore* a, Arvore* b) {
	if (a == NULL && b == NULL)
        return 1;

    if (a == NULL || b == NULL)
        return 0;

    if ( a->info == b->info && eh_espelho(a->esq, b->dir) && eh_espelho(a->dir, b->esq) )
        return 1;

    return 0;
}

int main (int argc, char *argv[]) {
    Arvore *a, *a_1, *a_2, *a_3, *a_4, *a_5;
    a_1 = constroi_arvore('d', cria_arvore_vazia(), cria_arvore_vazia());
    a_2 = constroi_arvore('b', cria_arvore_vazia(), a_1);
    a_3 = constroi_arvore('e', cria_arvore_vazia(), cria_arvore_vazia());
    a_4 = constroi_arvore('f', cria_arvore_vazia(), cria_arvore_vazia());
    a_5 = constroi_arvore('c', a_3, a_4);
    a  = constroi_arvore('a', a_2, a_5);
	printf("\n");
	imprime_arvore(a);
	printf("\n");

    Arvore *b;
    b = cria_espelho(a);
    printf("\n");
    imprime_arvore(b);
    printf("\n");

    if (eh_espelho(a, b)) {
        printf("\nA e B sao arvores espelho.\n");
    } else {
        printf("arvores A e B nao sao espelho.\n");
    }

    printf("\n");

    if (eh_espelho(a, a)) {
        printf("A e A sao arvores espelho.\n");
    } else {
        printf("A e A nao sao arvores espelho.\n");
    }

  return 0;
}
